
# `bb-gum`

Example application showing [babashka](https://babashka.org/) working with
[gum](https://github.com/charmbracelet/gum) - adding a little bit of that TUI goodness to your `bb` scripts! A `bbgum`!


> Note: Clojure noob here! If there's something that can be improved let me know! Thanks!
