(ns rattlin.gen
  (:require [clojure.string :as str]
            [faker.internet :as internet])

  (:import java.net.InetAddress
           java.nio.ByteBuffer))

(defn rand-10-addr-int []
  (let [min 2r00001010000000000000000000000001
        max 2r00001010111111111111111111111111
        range (- max min)]
    (+ min (rand-int range))))

(defn ip-address [n]
  (-> (.getByAddress InetAddress
                     (-> (ByteBuffer/allocate 4)
                         (.putInt n)
                         (.array)))
      .toString
      (str/replace-first "/" "")))

(defn generate [n]
  (map vector
       (take n (repeatedly #(ip-address (rand-10-addr-int))))
       (take n (cycle ["a" "b" "c" "d" "e"]))
       (take n (repeatedly internet/domain-name))))

(comment
  *e)
