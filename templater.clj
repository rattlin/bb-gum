(require
 '[babashka.process :refer [process check]]
 '[babashka.fs :as fs]
 '[rattlin.gum :as gum]
 '[clojure.data.csv :as csv]
 '[selmer.parser :as selmer])

(def db-path (or (System/getenv "TEMPLATER_DB") "some.db"))

(defn input-sql [initial-sql]
  (gum/write initial-sql "SELECT * FROM table"))

(defn exec-sql->csv [sql]
  (let [cmd (format "sqlite3 %s -header -csv" db-path)]
    (-> (process {:in sql :out :string} cmd)

        check
        :out)))

;; From: https://github.com/clojure/data.csv
(defn csv-data->maps [csv-data]
  (map zipmap
       (->> (first csv-data) ;; First row is the header
            (map keyword) ;; Drop if you want string keys instead
            repeat)
       (rest csv-data)))

(defn menu-select-inv
  "sql prompt, display results, confirm / loop"
  [initial-sql]
  (let [sql (input-sql initial-sql)
        csv (exec-sql->csv sql)
        _ (gum/table csv)
        continue (gum/confirm "Look good?")]
    (if continue
      {:csv csv :sql sql}
      (recur sql))))

(defn menu-choose-templates []
  (let [choices (->> (fs/list-dir "templates")
                     (map fs/file-name))
        chosen (gum/choose choices :no-limit true)
        chosen (map #(str (fs/path "templates" %)) chosen)]
    chosen))

(defn exec-template [outdir template-str template-path data]
  (let [template-name (fs/file-name template-path)
        outpath (fs/path outdir template-name)
        content (selmer/render template-str {:data data})]
    (spit (str outpath) content)))

(defn -main [& _]
  (gum/header "SQL Template Builder")
  (let [{:keys [csv]} (menu-select-inv "")
        csv (csv/read-csv csv)
        data (csv-data->maps csv)
        templates (menu-choose-templates)
        outdir (gum/input {:placeholder "path/to/out/dir"})]
    (println "Writing templates out to:" outdir)
    (fs/create-dirs outdir)
    (doseq [tmpl templates]
      (exec-template outdir (slurp tmpl) tmpl data))))

(when (= *file* (System/getProperty "babashka.file"))
  (apply -main *command-line-args*))

(comment
  (let [csv (exec-sql->csv "SELECT * FROM inventory")
        csv (csv/read-csv csv)
        data (csv-data->maps csv)
        template-str (slurp "templates/inventory.yml")
        content (exec-template "." template-str "templates/inventory.yml" data)]
    content)

  *e)
